# 📖 CHANGELOG
Log of major code changes

<br>

## [0.2.1] 2022-03-19
### 🐛 Fixed
+ Match objects of the 0.17.0 version
+ Keybinds are now spelled correctly
+ Fixed statusbar
+ Boots successfully


<br>
<br>


## [0.2.0] 2022-02-09
### ✨ Added
+ 2 More wallpapers in themes/wallpapers/
+ A python script called Qtilectl to automate processes
+ New bugs to be solved :D

### 🔥 Removed
+ Color apps feature

### 💥 Changes
+ Refactoring rules, keybinds (fragmenting into multiple files)
+ Moving colors, autostart functions to a \_\_int\_\_.py file
+ Separating widgets and style of the status bars and docks
+ MONITORS variable (from settings/theming) no longer necessary

### 🐛 Fixed
+ Statusbar import modules


<br>
<br>


## [0.1.1] 2022-01-16
### ✨ Added
+ A fallback wallpaper in themes/wallpapers/

### 💥 Changes
+ Moving statusbar config to themes dir
+ Renaming 'themes/color\_schemes/' -> 'themes/colors/'

### 🗑️ Deprecated
+ Colorize apps feature will be removed


<br>
<br>


## [0.1.0] 2021-07-23
### ✨ Added
+ Settings in the settings folder
+ Creating a horizontal and vertical dock
+ Creating 2 statusbar themes (Minla, Simpleone)
+ Creating color schemes themes (8)
+ Adding a pywal-like in python for some appplications (color\_apps)
+ Adding python script for changing the volume, brightness and taking screenshots
+ Adding a Xephyr script for launch Qtile in a nested session
+ Adding a script for launch applications at the qtile start
+ Adding some programs config inside the settings folder (redshift,picom,flashfocus,dunst)
