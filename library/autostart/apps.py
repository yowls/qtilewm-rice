#!/usr/bin/env python3

import subprocess
from time import sleep

import os
import sys
qtile_dir = os.path.expanduser("~/.config/qtile")
sys.path.append(qtile_dir)

from settings.autostart_apps    import *
from settings.paths             import log_apps
#  from library.helpers.debug      import notify_error


#################
# RUN FUNCTIONS #
#################

# TODO: better logging control (separation, date, name, stderr)
# TODO: if command not found, notify_error
def run(app):
    with open(log_apps, 'a+') as lf:
        subprocess.Popen(app.split(), stderr=lf)

def running(app):
    process = subprocess.run(['pgrep', '-f', app], capture_output=True, text=True)
    if process.returncode == 0:
        return True
    return False


#########
# START #
#########
def main():
    # Run applications if is not running
    for name, command in system.items():
        if not running(name):
            run(command)

    # TODO: add applications list

    # Run scripts in the list
    for x in range(len(scripts)):
        run(scripts[x])

    # If eww daemon is enabled, run its widgets windows
    if enable_eww:
        if not running("eww"):
            run("eww daemon")

            # Check if has completely loaded
            for _ in range(10):
                sleep(0.05)     # wait 50ms
                test  = subprocess.run(['eww', 'windows'], capture_output=True)
                ready = test.returncode

                if ready == 0:
                    break

        # TODO: close all widgets first
        # Load the window widgets
        for e in range(len(eww_windows)):
            open_widget = "eww open " + eww_windows[e]
            run(open_widget)
    return

if __name__ == "__main__":
    main()
