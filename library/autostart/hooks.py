import subprocess
from libqtile import hook, qtile
from libqtile.log_utils import logger

#   FUNCTIONS
#---------------------------
last_focus = None

def change_transparency(window, mode):
    """change window transparency based on his type."""

    if mode == "focus":
        if window.floating:
            window.cmd_opacity(.8)
        else:
            window.cmd_opacity(.95)
    else:
        if window.floating:
            window.cmd_opacity(.6)
        else:
            window.cmd_opacity(.9)


#   HOOKS
#---------------------------
@hook.subscribe.client_new
def new_window(window):
    """ Make new windows a little transparent """
    change_transparency(window, "focus")

#  @hook.subscribe.client_focus
#  def client_focus(window):
#      """ Change transparency on focus """
#      global last_focus
#
#      # Change transparency for unfocus window
#      if last_focus is not None and last_focus != window:
#          try:
#              change_transparency(last_focus, "unfocus")
#          except Exception:
#              pass    # ignore if error
#
#      # Save last focus window and
#      # change transparency for the current window
#      if last_focus != window:
#          last_focus = window
#          change_transparency(window, "focus")


# NOTE: depending on your version of qtile, this will not work
# @hook.subscribe.restart
#  def restart():
#      """ Send a notification when restart qtile """
#      msgid = 57980
#      icon = "/usr/share/icons/gnome/scalable/actions/view-refresh-symbolic.svg"
#      command = f"dunstify -a 'restartQtile' -u low -i {icon} -r {msgid} 'Restarting Qtile..' 'Please wait..'"
#      subprocess.run(command, shell=True)


# TODO: only send a notification when in a group change layout
# and reduce timeout to 2s
#  @hook.subscribe.layout_change
#  def change_layout():
#      """ Send a notification when changing layout """
#      msgid = 57981
#      icons = {}
#      command = f"dunstify -a 'changeGroup' -u low -i {icon} -r {msgid} 'Layout: [ .. ]'"
#      subprocess.run(command, shell=True)

# TODO: reduce timeout to 2s
@hook.subscribe.setgroup
def set_group():
    """ Send a notification when switching group """
    msgid = 57981
    icon = "/usr/share/icons/gnome/scalable/categories/applications-science-symbolic.svg"
    group_name = qtile.current_group.name
    group_label = qtile.current_group.label
    command = f"dunstify -a 'changeGroup' -u low -i {icon} -r {msgid} 'Group {group_name}: [ {group_label} ]'"
    subprocess.run(command, shell=True)


@hook.subscribe.enter_chord
def chord_on(name):
    """ Send a notification when activate the chord mode """
    msgid = 56931
    icon = "/usr/share/icons/gnome/scalable/devices/input-keyboard-symbolic.svg"
    command = f"dunstify -a 'chordModeOn' -u low -i {icon} -r {msgid} 'Chord Mode: {name}' 'Press ESC to exit'"
    subprocess.run(command, shell=True)

@hook.subscribe.leave_chord
def chord_off():
    """ Send a notification when disactivate the chord mode and close the other """
    msgid = 56932
    icon = "/usr/share/icons/gnome/scalable/devices/input-keyboard-symbolic.svg"
    command = f"dunstify -a 'chordModeOff' -u low -i {icon} -r {msgid} 'Chord Mode' 'Exited'"
    subprocess.run("dunstify -C 56931", shell=True)
    subprocess.run(command, shell=True)


# -------------
# Import this function to run hooks as side effect
def start_hooks():
    logger.warning("-> Hooks loaded fine :)")
