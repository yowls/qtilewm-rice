#!/usr/bin/env python3

import random
import subprocess

import os
import sys
qtile_dir = os.path.expanduser("~/.config/qtile")
sys.path.append(qtile_dir)

from settings.wallpaper     import *
from themes.colors.current  import colors
from library.helpers.debug  import notify_error
from settings.paths         import qtile_themes


# Set the wallpaper
def set(cmd):
    test = subprocess.run(cmd, shell=True)
    if test.returncode == 1:
        # Fallback wallpaper
        notify_error("Going fallback wallpaper..")
        fallwall = f"feh --no-fehbg --bg-fill --randomize {qtile_themes}wallpapers"
        subprocess.run(fallwall, shell=True)
    return


# Start point
def main():
    if method == "feh":
        # OPTION 1: Manage wallpaper with feh
        if randomize:
            set("feh --no-fehbg --bg-fill --randomize " + feh_dir)
        else:
            set("feh --no-fehbg --bg-fill " + feh_wall)

    # ===============================

    elif method == "xwinwrap":
        # OPTION 2: Manage animated wallpapers with xwinwrap
        #  go_xwinwrap(randomize, xwinwrap_dir, xwinwrap_wall)
        pass

    # ===============================

    else:
        # OPTION 3: Manage solid color wallpapers with hsetroot
        if randomize:
            wallpaper_color = random.choice(list(colors.values()))
        else:
            wallpaper_color = hset_wall

        set(f"hsetroot -solid '{wallpaper_color}'")
    return


if __name__ == '__main__':
    main()
