from libqtile.config        import Group, ScratchPad, DropDown
from settings.default_apps  import *
from settings.general       import group_names, group_layouts, group_labels, scratchpad_label
import settings.paths       as paths

# => Define groups
def init_groups(rules):
    groups = []
    for i in range(len(group_names)):
        groups.append(Group(
            name    = group_names[i],
            layout  = group_layouts[i],
            label   = group_labels[i],
            matches = rules[i]
        ))
    return groups


# => Define Scratchpads
def init_scratchpad():
    # general settings for scratchpads
    settings = {
        'on_focus_lost_hide' : True,
        'warp_pointer' : True,
        'opacity' : 0.9
    }

    top_side = {
        'height' : 0.45,
        'width' : 0.96,
        'x' : 0.015,
        'y' : 0.0,
        **settings
    }
    left_side = {
        'height' : 0.96,
        'width' : 0.45,
        'x' : 0.01,
        'y' : 0.0,
        **settings
    }
    center_side = {
        'height' : 0.9,
        'width' : 0.9,
        'x' : 0.05,
        'y' : 0.01,
        **settings
    }

    # Shortcut
    emacs_todo_org = TERMINAL_ALT +" -e emacs -nw " + paths.todo_org

    # Create all the scratchpads
    return ScratchPad("pad",
        dropdowns = [
            DropDown("terminal", TERMINAL_ALT,  **top_side),
            DropDown("chat",     COMUNICATION,  **left_side),
            DropDown("explorer", EXPLORER_ALT,  **top_side),
            DropDown("music",    MUSIC_PLAYER,  **center_side),
            DropDown("emacs",    emacs_todo_org,  **center_side),
            DropDown("pyshell",  TERMINAL_ALT +" -e python3", **center_side),
            DropDown("qshell",   TERMINAL_ALT +" -e qshell",  **top_side)
        ],
        label = scratchpad_label
    )
