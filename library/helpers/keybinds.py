#	FUNCTIONS
#---------------------------
# define functions to use in keybinds

# Switch focus between all windows in current group
#  def next_win(qtile):
#          pass

# Switch focus to the next/previous busy group
def next_busy_group(qtile):
	# It should work with
	# Screen.cmd_next_group(self?, skip_empty=False, skip_managed=False)

	# Work groups data as a list for easy iteration
	groups_data = qtile.cmd_groups()
	groups = list(groups_data.items())

	# only work for int group names :(
	current_group = int(qtile.current_group.name)

	# TODO: If using scratchpad -> groups -=1
	group_len = len(groups) - 1

	for i in range(group_len):
		if current_group == i:
			for k in range(group_len):
				next_group = current_group + k + 1
				if next_group >= group_len:
					next_group -= group_len
				if current_group == next_group + 1:
					# TODO: improve notification
					command = ["dunstify", "Error", "No group to switch"]
					qtile.cmd_spawn(command)
					break
				if groups[next_group][1]['windows'] != []:
					# there should be an easier way to do the same
					qtile.cmd_simulate_keypress(["mod4"], f"{next_group+1}")
					break

def prev_busy_group(qtile):
	# It should work with
	# Screen.cmd_next_group(self?, skip_empty=False, skip_managed=False)

	# Work groups data as a list for easy iteration
	groups_data = qtile.cmd_groups()
	groups = list(groups_data.items())

	# only work for int group names :(
	current_group = int(qtile.current_group.name)

	# TODO: If using scratchpad -> groups -=1
	group_len = len(groups) - 1

	for i in range(group_len):
		if current_group == i:
			for k in range(group_len):
				prev_group = current_group - k - 1
				if prev_group < 0:
					prev_group += group_len
				if current_group == prev_group - 1:
					# TODO: improve notification
					command = ["dunstify", "Error", "No group to switch"]
					qtile.cmd_spawn(command)
					break
				if groups[prev_group][1]['windows'] != []:
					# there should be an easier way to do the same
					qtile.cmd_simulate_keypress(["mod4"], f"{prev_group+1}")
					break

# move window to next and previous group
def window_to_prev_group(qtile):
	if qtile.current_window is not None:
		group = qtile.groups[qtile.groups.index(qtile.current_group) - 1].name
		qtile.current_window.togroup(group, switch_group=True)

def window_to_next_group(qtile):
	if qtile.current_window is not None:
		group = qtile.groups[qtile.groups.index(qtile.current_group) + 1].name
		qtile.current_window.togroup(group, switch_group=True)

# move window to next/previous monitor
def window_to_prev_screen(qtile):
	i = qtile.screens.index(qtile.current_screen)
	if i != 0:
		screen = qtile.screens[i - 1].group.name
		qtile.current_window.togroup(screen)

def window_to_next_screen(qtile):
	i = qtile.screens.index(qtile.current_screen)
	if i + 1 != len(qtile.screens):
		screen = qtile.screens[i + 1].group.name
		qtile.current_window.togroup(screen)

# Launch widget extension
def launch(qtile, extension):
	qtile_dir = "~/.config/qtile/"
	qtile.cmd_spawn(qtile_dir+extension ,shell=True)

# Launch script from script folder
def launch_script(qtile, script, comand):
	script_dir = "/home/yowls/.config/qtile/scripts"
	qtile.cmd_spawn([f"{script_dir}/{script}", comand])

# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
