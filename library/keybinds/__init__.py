from library.keybinds.mouse		import init_mouse
from library.keybinds.qtile		import qtile_keys
from library.keybinds.focus		import focus_keys
from library.keybinds.layout		import layout_keys
from library.keybinds.move		import move_keys
from library.keybinds.resize		import resize_keys
from library.keybinds.screen		import screen_keys
from library.keybinds.group		import group_keys
from library.keybinds.system		import system_keys
from library.keybinds.application	import application_keys


################
# SPECIAL KEYS #
################
#  modifier_keys = {
#     'M': 'mod4',
#     'A': 'mod1',
#     'S': 'shift',
#     'C': 'control',
#  }


############
# KEYBINDS #
############

def init_keybinds(group):
	keys = []
	keys.extend( qtile_keys()      )
	keys.extend( focus_keys()      )
	keys.extend( layout_keys()     )
	keys.extend( move_keys()       )
	keys.extend( resize_keys()     )
	keys.extend( screen_keys()     )
	keys.extend( group_keys(group) )
	keys.extend( system_keys()     )
	keys.extend( application_keys())
	return keys

# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
