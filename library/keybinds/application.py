from libqtile.config		import EzKey as Key, KeyChord
from libqtile.command		import lazy
from settings.default_apps	import *
from settings.paths		import qtile_log, todo_org

################
# APPLICATIONS #
################
# FIXME: replace mod4

def application_keys():
	# application shortcuts
	emacs_cmd = "emacsclient -c -a 'emacs' "

	return [
		# PREFERED APPLICATIONS
		# key chord mode, emacs like
		# FIXME: replace mod4
		KeyChord(["mod4"], "g", [
			# => Browser
			Key("b",
				lazy.spawn(BROWSER),
				desc = "Launch main web browser"
			),
			Key("S-b",
				lazy.spawn(BROWSER_ALT),
				desc = "Launch alternative web browser"
			),

			# => File manager
			Key("e",
				lazy.spawn(EXPLORER),
				desc = "Launch file explorer"
			),
			Key("S-e",
				lazy.spawn(EXPLORER_ALT),
				desc = "Launch alternative file explorer"
			),

			# => Text Editors
			Key("v",
				lazy.spawn(EDITOR_GUI),
				desc = "Launch graphical editor"
			),
			Key("S-v",
				lazy.spawn(EDITOR_CMD),
				desc = "Launch CLI editor"
			),

			# => Others
			Key("d",
				lazy.spawn("discord"),
				desc="Launch Discord client"
			),
			Key("t",
				lazy.spawn("telegram-desktop"),
				desc="Launch Telgram desktop"
			),
			Key("c",
				lazy.spawn("clementine"),
				desc="Launch Clementine music player"
			),
			Key("l",
				lazy.spawn(EDITOR_CMD + qtile_log),
				desc="Open log file"
			)
		]),

		# => Terminals
		Key("M-<Return>",
			lazy.spawn(TERMINAL),
			desc = "Launch main terminal"
		),
		Key("C-A-t",
			lazy.spawn(TERMINAL_ALT),
			desc = "Launch alternative terminal"
		),


		# APPLICATIONS

		# => Emacs
		# M + e + {e,t,c,q}
		KeyChord(["mod4"], "e", [
			Key("e",	lazy.spawn(emacs_cmd),				desc = "Launch Emacs"),
			Key("t",	lazy.spawn(emacs_cmd + todo_org),		desc = "My To-Do list in org mode"),
			Key("c",	lazy.spawn(emacs_cmd + "~/.emacs.d"),		desc = "Open Emacs config"),
			Key("q",	lazy.spawn(emacs_cmd + "~/.config/qtile"),	desc = "Qtile config in emacs"),
		]),

		# => System monitor
		Key("C-<Escape>",
			lazy.spawn(TERMINAL_ALT + " -e htop"),
			desc = "System Monitor"
		),

		# => Screenshots
		Key("<Print>",
			lazy.spawn("screenshot selective"),
			desc = "Take partial screenshot and save it to clipboard"
		),
		Key("M-<Print>",
			lazy.spawn("screenshot full"),
			desc = "Take full screenshot and save in ~/Pictures folder"
		),
		Key("A-<Print>",
			lazy.spawn("screenshot partial"),
			desc = "Save partial screenshot in ~/Pictures folder"
		),
		Key("M-S-<Print>",
			lazy.spawn("screenshot floating"),
			desc = "Take partial screenshot and open it with feh"
		),
		#  Key("C-<Print>",
		#          lazy.spawn("screenshot_menu"),
		#          desc = "Show screenshot menu"
		#  ),

		# => Record
		#  Key("M-<F10>",
		#          lazy.spawn("record toggle"),
		#          desc="Start/Stop recording"
		#  ),

		# => Desktop menu
		Key("A-s",
			lazy.spawn("run_xmenu"),
			desc = "Show desktop menu"
		),

		# => Launcher
		Key("A-<space>",
			lazy.spawn("rofi -show drun"),
			desc = "Rofi - Applications"
		),
		Key("A-x",
			lazy.spawn("rofi -show run"),
			desc = "Rofi - Run script"
		),
		Key("M-w",
			lazy.spawn("rofi -show window"),
			desc = "Rofi - Windows list"
		),

		# => Clipboard ( Greenclip )
		Key("M-c",
			lazy.spawn("rofi -modi 'clipboard:greenclip print' -show clipboard -run-command '{cmd}'"),
			desc = "Launch Greenclip"
		),
		# Key("A-c",		lazy.spawn("rm ~/.cache/greenclip.history"), desc="Clear clipboard history")
	]
# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
