from libqtile.config	import EzKey as Key
from libqtile.command	import lazy

#########
# FOCUS #
#########

def focus_keys():
	return [
		Key("M-h",
			lazy.layout.left(),
			desc = "Move focus left"
		),
		Key("M-j",
			lazy.layout.down(),
			desc = "Move focus down in stack pane"
		),
		Key("M-k",
			lazy.layout.up(),
			desc = "Move focus up in stack pane"
		),
		Key("M-l",
			lazy.layout.right(),
			desc = "Move focus right"
		),

		Key("A-<Tab>",
			lazy.layout.next(),
			desc = "Switch window focus to next pane(s) of stack"
		),
		Key("A-S-<Tab>",
			lazy.layout.previous(),
			desc = "Switch window focus to previous pane(s) of stack"
		),

		#  Key("M-<Tab>",
		#          lazy.?,
		#          desc = "Switch focus to last visited monitor"
		#  ),
		#  Key("M-S-+",
		#          lazy.?,
		#          desc = "Increase window opacity"
		#  ),
		#  Key("M-S--",
		#          lazy.?,
		#          desc = "Decrease window opacity"
		#  ),

		Key("M-q",
			lazy.window.kill(),
			desc = "Kill focused window"
		)
	]
# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
