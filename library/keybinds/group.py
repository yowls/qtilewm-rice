from libqtile.config		import EzKey as Key
from libqtile.command		import lazy
from library.helpers.keybinds	import *

#########
# GROUP #
#########

def group_keys(groups):
	keys = [
		# Switch focus with arrows
		Key("M-<Right>",
			lazy.screen.next_group(),
			desc = "Switch focus to the group on the right"
		),
		Key("M-<Left>",
			lazy.screen.prev_group(),
			desc = "Switch focus to the group on the left"
		),
		Key("M-<Up>",
			lazy.function(next_busy_group),
			desc = "Switch focus to the next busy group"
		),
		Key("M-<Down>",
			lazy.function(prev_busy_group),
			desc = "Switch focus to the previous busy group"
		),

		# Rotate focus
		Key("M-<bar>",
			lazy.screen.toggle_group(),
			desc = "Switch to the last visited group"
		),

		# Move window to another group with arrows
		Key("M-S-<Right>",
			lazy.function(window_to_next_group),
			desc = "Move window to the group on the right"
		),
		Key("M-S-<Left>",
			lazy.function(window_to_prev_group),
			desc = "Move window to the group on the left"
		),
		Key("M-S-<period>",
			lazy.function(window_to_next_screen),
			desc = "Move window to the screen on the left"
		),
		Key("M-S-<comma>",
			lazy.function(window_to_prev_screen),
			desc = "Move window to the screen on the left"
		)
	]

	# Switch focus to group
	# Iterate to the penultimate, because scratchpad
	for i in groups[:-1]:
		keys.extend([
			# => Switch to group
			Key(f"M-{i.name}",
				lazy.group[i.name].toscreen(),
				desc = f"Switch to group {i.name}"
			),

			# => Switch to & move focused window to group
			Key(f"M-S-{i.name}",
				lazy.window.togroup(i.name, switch_group=True),
				desc = f"Switch to & move focused window to group {i.name}"
			),
		])
	return keys

# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
