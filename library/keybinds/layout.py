from libqtile.config	import EzKey as Key, KeyChord
from libqtile.command	import lazy

###########
# LAYOUTS #
###########

def layout_keys():
	return [
		Key("M-<space>",
			lazy.next_layout(),
			desc = "Next layout"
		),
		Key("M-S-<space>",
			lazy.prev_layout(),
			desc = "Previous layout"
		),

		Key("M-C-<space>",
			lazy.layout.rotate(),
			desc = "Swap panes of split stack"
		),

		Key("M-S-<Return>",
			lazy.layout.toggle_split(),
			desc = "Toggle between split and unsplit sides of stack"
		),

		Key("M-s",
			lazy.window.toggle_floating(),
			desc = "Toggle floating mode"
		),
		Key("M-m",
			lazy.window.toggle_floating(),
			desc = "Toggle floating mode"
		),

		Key("M-f",
			lazy.window.toggle_fullscreen(),
			desc = "Toggle Fullscreen mode"
		),

		# FIXME: replace mod4
		KeyChord(["mod4"], "t", [
			Key("1",	lazy.group.setlayout('max'),		desc = "Change to MAX layout"),
			Key("2",	lazy.group.setlayout('monadtall'),	desc = "Change to MONAD-TAll layout"),
			Key("3",	lazy.group.setlayout('monadwide'),	desc = "Change to MONAD-WIDE layout"),
			Key("4",	lazy.group.setlayout('tile'),		desc = "Change to TILE layout"),
			Key("5",	lazy.group.setlayout('verticaltile'),	desc = "Change to VERTICAL-TILE layout"),
			Key("6",	lazy.group.setlayout('ratiotile'),	desc = "Change to RATIO-TILE layout"),
			Key("7",	lazy.group.setlayout('bsp'),		desc = "Change to BSP layout"),
			Key("8",	lazy.group.setlayout('columns'),	desc = "Change to COLUMNS layout"),
			Key("9",	lazy.group.setlayout('stack'),		desc = "Change to STACK layout"),
			Key("0",	lazy.group.setlayout('floating'),	desc = "Change to FLOATING layout"),
		])
	]
# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
