from libqtile.config		import EzClick as Click, EzDrag as Drag
from libqtile.command		import lazy
from library.helpers.keybinds	import *


##############
# MOUSEBINDS #
##############
# NOTE: detect mouse buttons number with: xev | grep button

def init_mouse():
	return [
		# Move window in floating mode
		Drag("M-1",
			lazy.window.set_position_floating(),
			start = lazy.window.get_position()
		),

		# Resize window in floating mode
		Drag("A-1",
			lazy.window.set_size_floating(),
			start = lazy.window.get_size()
		),

		Drag("M-3",
			lazy.window.set_size_floating(),
			start = lazy.window.get_size()
		),

		# Set window to focus ?
		Click("M-2",
			lazy.window.bring_to_front()
		),

		# Switch to next/prev group with M + mouse scroll
		Click("M-4",
			lazy.screen.next_group()
		),
		Click("M-5",
			lazy.screen.prev_group()
		),

		# Rotate window focus in the group with A + side buttons
		Click("A-9",
			lazy.layout.next()
		),
		Click("A-8",
			lazy.layout.previous()
		),

		# Switch to next/prev busy group
		Click("M-9",
			lazy.function(next_busy_group)
		),
		Click("M-8",
			lazy.function(prev_busy_group)
		),

		# Move window to next/prev group
		Click("M-S-9",
			lazy.function(window_to_next_group)
		),
		Click("M-S-8",
			lazy.function(window_to_prev_group)
		),

		# Move window to next/prev monitor
		Click("M-C-9",
			lazy.function(window_to_next_screen)
		),
		Click("M-C-8",
			lazy.function(window_to_prev_screen)
		),

		# Change window opacity
		Click("M-A-4",
			lazy.window.up_opacity()
		),
		Click("M-A-5",
			lazy.window.down_opacity()
		)
	]
# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
