from libqtile.config	import EzKey as Key, KeyChord
from libqtile.command	import lazy

#########
# QTILE #
#########

def qtile_keys():
	return [
		# => Basic wm
		Key("M-C-q",
			lazy.shutdown(),
			desc = "Shutdown qtile"
		),
		Key("M-C-r",
			lazy.restart(),
			desc = "Restart qtile"
		),

		# => Prompt
		Key("A-<F2>",
			lazy.spawncmd(),
			desc = "Spawn a command using a prompt widget"
		),
		#  Key("M-r",
		#          lazy.run_extension(extension.DmenuRun(**_dmenu_run_settings)),
		#          desc = "Spawn a command using DmenuRun"
		#  ),

		# => Scratchpad
		Key("A-<bar>",
			lazy.group["pad"].dropdown_toggle("terminal"),
			desc = "Scratchpad -> Terminal"
		),
		Key("M-<F4>",
			lazy.group["pad"].dropdown_toggle("chat"),
			desc = "Scratchpad -> Telegram"
		),
		Key("M-<F7>",
			lazy.group["pad"].dropdown_toggle("explorer"),
			desc = "Scratchpad -> Explorer"
		),
		Key("M-<F8>",
			lazy.group["pad"].dropdown_toggle("music"),
			desc = "Scratchpad -> Music"
		),
		Key("M-<F9>",
			lazy.group["pad"].dropdown_toggle("emacs"),
			desc = "Scratchpad -> Emacs"
		),
		Key("M-<F10>",
			lazy.group["pad"].dropdown_toggle("pyshell"),
			desc = "Scratchpad -> Python3"
		),
		Key("M-<F11>",
			lazy.group["pad"].dropdown_toggle("qshell"),
			desc = "Scratchpad -> Qshell"
		),

		# => Statusbar
		# toggle status bar (show/hide)
		# FIXME: replace mod4
		KeyChord(["mod4"], "y", [
			Key("a",	lazy.hide_show_bar("all"),	desc="Toggle ALL statusbar state"),
			Key("h",	lazy.hide_show_bar("left"),	desc="Toggle TOP statusbar state"),
			Key("j",	lazy.hide_show_bar("bottom"),	desc="Toggle BOTTOM statusbar state"),
			Key("k",	lazy.hide_show_bar("top"),	desc="Toggle LEFT statusbar state"),
			Key("l",	lazy.hide_show_bar("right"),	desc="Toggle RIGHT statusbar state"),
		])
	]
# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
