from libqtile.config	import EzKey as Key
from libqtile.command	import lazy

##########
# RESIZE #
##########

def resize_keys():
	return [
		Key("M-S-n",
			lazy.layout.normalize(),
			desc = "Restore window space"
		),

		Key("M-S-m",
			lazy.layout.maximize(),
			desc = "Maximize window space"
		),

		Key("M-S-k",
			lazy.layout.grow(),		# general
			lazy.layout.grow_up(),		# bsp
			lazy.layout.increase_ratio(),	# tile
			desc = "Grow window space"
		),

		Key("M-S-j",
			lazy.layout.shrink(),		# general
			lazy.layout.grow_down(),	# bsp
			lazy.layout.decrease_ratio(),	# tile
			desc = "Shrink window space"
		),

		Key("M-S-h",
			lazy.layout.grow_left(),
			desc = "Grow window space to the left"
		),
		Key("M-S-l",
			lazy.layout.grow_right(),
			desc = "Grow window space to the right"
		),
	]
# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
