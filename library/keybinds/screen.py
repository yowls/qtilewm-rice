from libqtile.config	import EzKey as Key
from libqtile.command	import lazy

##########
# SCREEN #
##########

def screen_keys():
	return [
		# Focus by number
		Key("A-1",
			lazy.to_screen(0),
			desc = "Move focus to monitor 1"
		),
		Key("A-2",
			lazy.to_screen(1),
			desc = "Move focus to monitor 2"
		),
		Key("A-3",
			lazy.to_screen(2),
			desc = "Move focus to monitor 3"
		),
		Key("A-4",
			lazy.to_screen(3),
			desc = "Move focus to monitor 4"
		),

		# Rotate focus
		# TODO: send window to other monitor with A+S+{1,2,3}
		Key("M-<period>",
			lazy.next_screen(),
			desc = "Move focus to next monitor"
		),
		Key("M-<comma>",
			lazy.prev_screen(),
			desc = "Move focus to previous monitor"
		)
	]
# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
