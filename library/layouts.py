from libqtile           import layout
from settings.general   import layouts_list
from settings.theming   import window_margin, window_border

# => Set colors of frames by chosen color scheme
def init_layout_theme(colors):
    return {
        'margin': window_margin,
        'border_width': window_border,
        'border_focus': colors['accent'],
        'border_normal': colors['bg']
    }


# => Set the color scheme by the name of theme
def init_treetab_theme(color_scheme):
    treetab_settings = {
        'fontsize' : 14,
        'section_fontsize' : 16,
        'vspace' : 10,
        'padding_y' : 4,
        'padding_x' : 2,
        'padding_left' : 0,
        'margin_y' : 10,
        'border_width' : 20,
        'panel_width' : 200,
        'section_top' : 10,
        'section_left' : 10,
        'section_bottom' : 6,
        'section_padding' : 10,
        'level_shift' : 10,
        'previous_on_rm' : True
    }
    treetab_settings['bg_color']    = color_scheme['bg']
    treetab_settings['active_bg']   = color_scheme['bg']
    treetab_settings['inactive_bg'] = color_scheme['bg']
    treetab_settings['active_fg']   = color_scheme['fg']
    treetab_settings['inactive_fg'] = color_scheme['fg-inactive']
    treetab_settings['section_fg']  = color_scheme['accent']
    treetab_settings['urgent_fg']   = color_scheme['bg']
    treetab_settings['urgent_bg']   = color_scheme['alert']

    treetab_settings['sections'] = ['I', 'II', 'III']
    treetab_settings['font'] = "sans"

    return treetab_settings


# => Set layouts to use
def init_layout(theme, colors):
    layouts = []
    if "Max" in layouts_list:
        layouts.append(layout.Max())

    if "MonadTall" in layouts_list:
        layouts.append(layout.MonadTall(**theme))

    if "MonadWide" in layouts_list:
        layouts.append(layout.MonadWide(**theme))

    if "Tile" in layouts_list:
        layouts.append(layout.Tile(**theme))

    if "verticalTile" in layouts_list:
        layouts.append(layout.VerticalTile(**theme))

    if "RatioTile" in layouts_list:
        layouts.append(layout.RatioTile(**theme))

    if "Bsp" in layouts_list:
        layouts.append(layout.Bsp(**theme))

    if "Columns" in layouts_list:
        layouts.append(layout.Columns(**theme))

    if "Matrix" in layouts_list:
        layouts.append(layout.Matrix(**theme))

    if "Zoomy" in layouts_list:
        layouts.append(layout.Zoomy(**theme))

    if "Stack" in layouts_list:
        layouts.append(layout.Stack(num_stacks=2, **theme))

    if "TreeTab" in layouts_list:
        treetab = init_treetab_theme(colors)
        layouts.append(layout.TreeTab(**treetab))

    if "Floating" in layouts_list:
        layouts.append(layout.Floating(**theme))

    return layouts

