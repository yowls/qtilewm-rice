from libqtile.config import Match

# Run the utility of `xprop` to see the details of an X client.
# options: <title>, <wm_class>, <role>, <wm_type>, <wm_instance_class>, <net_wm_pid>

# Define whats open in X group
def groups_match():
	return [
		[ # Group 1
			Match(wm_class = "kitty"),
			Match(wm_class = "konsole")
		],
		[ # Group 2
			Match(wm_class = "Firefox"),
			Match(wm_class = "Chromium-browser")
		],
		[ # Group 3
			Match(wm_class = "VSCodium"),
			Match(wm_class = "Atom"),
			Match(wm_class = "Emacs")
		],
		[ # Group 4
			# Match(wm_class = "TelegramDesktop"),
			# Match(wm_class = "Pcmanfm"),
			Match(wm_class = "dolphin"),
			Match(wm_class = "thunar"),
			Match(wm_class = "okular"),
			Match(wm_class = "kate")
		],
		[ # Group 5
			Match(wm_class = "discord"),
			# Match(wm_class = "zoom"),
			Match(wm_class = "Microsoft Teams - Preview")
		],
		[ # Group 6
			Match(wm_class = "Gimp-2.10"),
			Match(wm_class = "Libreoffice"),	# fix name
			Match(wm_class = "WPS")				# fix name
		],
		[ # Group 7
			Match(wm_class = "Thunderbird"),
			Match(wm_class = "Kmail"),
			Match(wm_class = "KOrganizer")
		],
		[ # Group 8
			Match(wm_class = "DeaDBeeF"),		# fix name
			Match(wm_class = "Clementine"),
			Match(wm_class = "Spotify")
		],
		[ # Group 9
			Match(wm_class = "KeePassXC"),
			Match(wm_class = "Bitwarden")
		],
		[ # Group 10
			Match(wm_class = "obs"),
			Match(wm_class = "Kamoso")
		]
	]
# vim: tabstop=4 shiftwidth=4 softtabstop=0 noexpandtab nowrap
