#!/bin/bash

# Define colors
colorRed="\033[0;31m"
colorYellow="\033[0;33m"
colorGreen="\033[0;32m"
colorBlue="\033[0;34m"
colorGray="\033[0;37m"
colorEnd="\033[0m"

# Welcome message
function welcomeMessage(){
	echo -e $colorRed
	echo "  ______     __     ______     ______        ______     ______   __     __         ______    "
	echo " /\  == \   /\ \   /\  ___\   /\  ___\      /\  __ \   /\__  _\ /\ \   /\ \       /\  ___\   "
	echo " \ \  __<   \ \ \  \ \___  \  \ \  __\      \ \ \/\_\  \/_/\ \/ \ \ \  \ \ \____  \ \  __\   "
	echo "  \ \_\ \_\  \ \_\  \/\_____\  \ \_____\     \ \___\_\    \ \_\  \ \_\  \ \_____\  \ \_____\ "
	echo "   \/_/ /_/   \/_/   \/_____/   \/_____/      \/___/_/     \/_/   \/_/   \/_____/   \/_____/ "
	echo -e $colorEnd
	printf "%$(tput cols)s" " " | tr " " "_"
}


# Check if Qtile is installed
function checkQtile() {
	if [ !$(qtile --version) ]; then
		echo -e "$colorRed[WARNING]$colorEnd You dont have Qtile installed yet"
		echo -e "\t ->$colorYellow http://docs.qtile.org/en/latest/manual/install/index.html$colorEnd"
		echo -e "- - - - - - - - - - - - - - - -"
	fi
}


# Check dependencies, otherwise suggest installing it
function checkRequieredDependencies() {
	declare -a app_list

	# Compositor
	command -v picom >/dev/null 2>&1 || app_list+=("picom compositor")

	# Launcher
	command -v rofi >/dev/null 2>&1 || app_list+=("rofi launcher")

	# Wallpaper
	if [ ! $(command -v hsetroot) ] && [ ! $(command -v feh) ] && [ ! $(command -v nitrogen) ];then
		app_list+=("hsetroot, feh or nitrogen for Wallpapers")
	fi

	# Notification
	command -v dunst >/dev/null 2>&1 || app_list+=("dunst notification daemon")

	# Return not installed apps
	if [ ${#app_list[@]} -eq 0 ];then
		echo -e "$colorGreen All the requiered dependencies satisfied$colorEnd"
	else
		echo "$colorRed[ALERT]$colorEnd Not installed:"
		echo -e "$colorRed ${app_list[@]} $colorEnd"
	fi
}

function checkOptionalDependencies() {
	echo "check >> https://gitlab.com/yowls/qtwm/-/wikis/Install"
}


# Backup qtile folder if there is already one
function backup() {
	if [ -d "$HOME/.config/qtile" ]; then
		echo -e "$colorRed[WARNING]$colorEnd Detecting existent qtile config"
		echo -e "The folder will move to $colorGreen(~/.config/qtile-old)$colorEnd"

		while [[ "$backup_opt" != "y" && "$backup_opt" != "Y" && "$backup_opt" != "n" ]]
		do
			echo -e -n "Proceed? $colorYellow(Y/n)$colorEnd: "
			read -r backup_opt
			if [ $backup_opt = "y" ] || [ $backup_opt = "Y" ]; then
				mv ~/.config/qtile ~/.config/qtile-old
				echo -e "Mooved old config$colorRed (~/.config/qtile)$colorEnd -> $colorGreen(~/.config/qtile-old)$colorEnd"
				echo -e "- - - - - - - - - - - - - - - -"

			elif [ $backup_opt = "n" ]; then
				echo -e "$colorRed* Directory conflict. Aborting$colorEnd"
				exit 1
			fi
		done
	fi
}


# Download repository
function implement() {
	echo -e "Clone with: \n\t(1) HTTPs $colorGray(default)$colorEnd \n\t(2) SSH"
	read -r -p "Option: " clone_opt

	if [ $clone_opt -eq 2 ]; then
		echo "Cloning with$colorGreen ssh$colorEnd into ~/.config/qtile .."
		git clone git@gitlab.com:yowls/qtwm.git ~/.config/qtile
	else
		echo "Cloning with$colorGreen https$colorEnd into ~/.config/qtile .."
		git clone --depth 1 https://gitlab.com/yowls/qtwm/ ~/.config/qtile
	fi
}


function main() {
	welcomeMessage
	checkQtile
	checkRequieredDependencies
	checkOptionalDependencies
	backup
	implement
}
# main
welcomeMessage
