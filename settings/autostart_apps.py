import settings.paths as paths
configs = paths.qtile_settings + "configs"

##########
# SYSTEM #
##########
# Only will run applications that currently are not running
#
# < key: name of program (for check if is running)
# > value: command to run

# TODO: replace notify-send with dunstify
system = {
    # DESKTOP ENVIRONMENT INTEGRATION
    # "xfce-mcs-manager" : "xfce-mcs-manager",

    # POLICYKIT | PAM | KEYRING
    "polkit-gnome" : "/usr/libexec/polkit-gnome-authentication-agent-1",
    # "gnome-keyring-daemon" : "eval $(gnome-keyring-daemon -s --components=pkcs11,secrets,ssh,gpg)",

    # X11 COMPOSITOR
    # "picom"      : "picom -b --experimental-backends --backend glx --vsync",
    "picom"      : f"picom --config {configs}/picom_jonaburg.conf",
    "flashfocus" : f"flashfocus -c {configs}/flashfocus.yml",

    # LOCKSCREEN
    # NOTE: require systemd
    "xidlehook" : "xidlehook --not-when-fullscreen --not-when-audio \
            --timer 300 'light -S 5' 'light -S 10' \
            --timer 590 'notify-send Suspending.. ' 'light -S 10' \
            --timer 600 'systemctl suspend' 'light -S 10' ",

    # DAEMONS
    "redshift"  : f"redshift -c {configs}/redshift.conf",
    "dunst"     : f"{paths.qtile_dir}scripts/apps/launch_dunst",
    "greenclip" : "greenclip daemon"
}


################
# APPLICATIONS #
################
# Run applications only at the start of the window manager
# and only if are not running

applications = {
    "daemons"   : {
        "mpd"     : "mpd",
        # "urxvtd"  : "urxvtd -q -o -f",
        "emacs"   : "emacs --daemon",
        # "pcmanfm" : "pcmanfm --daemon-mode",
        "thunar"  : "thunar --daemon",
    },

    "graphical" : {
        "kitty"   : "kitty",
        "firefox" : "firefox",
        # "telegram-desktop" : "telegram-desktop",
        # "megasync": "megasync",
        # "transmission" : "transmission"
    }
}


###################
# DESKTOP WIDGETS #
###################

# value: True | False (bool)
enable_eww = False

eww_windows = (
    "min_clock",
)


##################
# CUSTOM SCRIPTS #
##################
# run this lines in a shell at login
# wont check if is running and wont do any log

scripts = (
    "xrdb -merge ~/.Xresources",
)
