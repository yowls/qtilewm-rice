import os

#   APPLICATIONS OF CHOICE
#---------------------------
# try to use environment variables first

# Terminals
TERMINAL	= os.getenv("TERMINAL", "kitty")
TERMINAL_ALT	= 'urxvt'

# Text Editors
EDITOR_CLI	= os.getenv("EDITOR", "nvim")
EDITOR_CMD	= f"{TERMINAL} -e {EDITOR_CLI} "
EDITOR_GUI	= 'codium'

# File manager
EXPLORER	= os.getenv("EXPLORER", "thunar")
EXPLORER_ALT	= 'pcmanfm'

# Browser
# TODO: fix flatpak version
BROWSER		= os.getenv("BROWSER", "firefox")
BROWSER_ALT	= 'chromium'

# Others
MUSIC_PLAYER	= "clementine"
COMUNICATION	= "telegram-desktop"

# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
