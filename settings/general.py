# {{{ LAYOUTS
# ====================

# => Comment or Uncomment for Disable/Enable layout
# available on all monitors
layouts_list = [
    "Max",
    "MonadTall",
    "MonadWide",
    "Tile",
    # "VerticalTile",
    # "RatioTile",
    # "Bsp",
    # "Columns",
    # "Matrix",
    # "Zoomy",
    # "Stack",
    # "TreeTab",
    "Floating"
]

# }}}}


#########################################################


# {{{ GROUPS
# ====================

# NOTE: DEPRECATED
# Number of Groups
# names are the buttons you press to switch to
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

# => Groups labels
# labels are what you see in the groups widget in the bar
group_labels = ["", "", "", "", "", "", "", "", "", ""]
#group_labels = ["", "", "喝", "", "", "卑", "", "柳", "謁", "ﴰ"]
#group_labels = ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十"]

# => Scratchpad
scratchpad_label = ""

# => Groups layouts
# define what layout use in what group
# see all available names in layouts_list
group_layouts = [
    "monadtall",    # 1
    "max",          # 2
    "monadwide",    # 3
    "tile",         # 4
    "max",          # 5
    "monadwide",    # 6
    "monadwide",    # 7
    "monadwide",    # 8
    "tile",         # 9
    "monadtall"     # 10
]

# }}}
