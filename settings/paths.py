import os

# DIRECTORIES
# ==============
# useful folder locations

# FIXME: use system xdg dirs
# User dirs
home		=  os.getenv("HOME", default = "") + "/"
desktop		=  home + "Desktop"
downloads	=  home + "Downloads"
documents	=  home + "Documents"
pictures	=  home + "Pictures"
videos		=  home + "Videos"

# Config
config_dir	= os.getenv("XDG_CONFIG_HOME", home+".config") + "/"
qtile_dir	= config_dir + "qtile/"
qtile_themes	= qtile_dir + "themes/"
qtile_settings	= qtile_dir + "settings/"
sscripts	= qtile_dir + "scripts/system/"

# Cache
cache_dir	= os.getenv("XDG_CACHE_HOME", home+".cache") + "/"
qtile_cache	= cache_dir + "qtile/"

# Data
data_dir	= os.getenv("XDG_DATA_HOME", home+".local/share") + "/"
qtile_data	= data_dir + "qtile/"


# FILES
# ==============
# useful file locations

# To-do list with org mode
todo_org	= '/backup/org/linux.org'

# Qtile logs
# NOTE: at the moment, is just for the keybind to open it
qtile_log	= qtile_data + "qtile.log"
log_stdout	= qtile_data + "stdout.log"
log_stderr	= qtile_data + "stderr.log"
log_apps	= qtile_data + "log_apps.log"

# vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap
