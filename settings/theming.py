# {{{ ⚙️ GENERAL
# ====================

window_margin = 6   # gap between windows (px)
window_border = 3   # border width (px)

# }}}


#########################################################


# {{{ 🎨 COLOR SCHEME
# ====================

# Randomize the color scheme on every restart
# value: True | False (type bool)
color_scheme_random = False

# List of available themes
color_scheme_themes = [
    "greene",       # [0] Focus on green
    "nihilist",     # [1] Dark blue
    "soft",         # [2] Softy colors
    "cloud",        # [3] Blue light
    "storm",        # [4] Blue darker
    "warm",         # [5] Warm colors
    "whiter",       # [6] A pure dark palette
    "dracula",      # [7] Dracula colors
]

# Set color scheme
# value: string name from above or array item
color_scheme = "dracula"

# }}}


#########################################################


# {{{ 🚥 STATUS BAR
# ====================

# Do you like status bar?
# value: True | False (type bool)
enable_statusbar = True

# Randomize theme on every restart
# value: True | False (type bool)
random_statusbar = False

# List of available themes
statusbar_themes = [
    "Simpleone",    # {0}. Simple bar at bottom
    "Minla",        # {1}. Material bar at top
    "Partitus",     # {2}. Double compact bars
]

# Set status bar theme
# value: string name from above or array item
statusbar = "Minla"

# How much bloated do you want?
# these are variants, are to add multiples status bars in the same theme
# values:
#   'simple'       - Repeat the same status bar for each monitor
#   'multi'        - Two bars, one for main monitor and other bar for the rest of monitors
#   'compost'      - Dual bar in main monitor + one_line for other monitors
statusbar_variant = "simple"

# }}}


#########################################################


# {{{ 🧰 DOCKS
# ====================

# Enable or disable dock bar
# value: True | False (type bool)
enable_dock = False

# Randomize theme on every restart
# value: True | False (type bool)
random_dock = False

# List of available theme names
dock_themes = [
    "vertical",     # {0}. One bar with dock style at left side
    "horizontal"    # {1}. One bar with dock style at Bottom side
]

# Set dock theme
# this only applies for main monitor
# value: string name from above or array item
dock = "vertical"

# }}}
