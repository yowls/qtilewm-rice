wall_dir = "~/library/Wallpaper/"

#   Application backend
#---------------------------
# Set the application to work with
# works for all modes
#> values:
#   'feh'             - General images jpg/png
#   'xwinwrap'        - Animated gif/mp4
#   'hsetroot'        - Solid colors
method = "feh"


#   Slideshow mode options
#---------------------------
# NOTE: not implemented yet
# Allow to change wallpaper in periods of time
# can be combined with random mode
#> value: int in minutes
slideshow = 60


#   Random mode options
#---------------------------
# + for feh/xwinwrap select a random image on a folder
# + for hsetroot set a random color based on chosen palette
#> value: True, False
randomize = False

# Select directory for randomize
# TODO: allow multiples paths
feh_dir         = wall_dir + "Photography"
xwinwrap_dir    = wall_dir + "nimated"


#   Static mode options
#---------------------------
# Select wallpaper by path
# only work if randomize = False
# TODO: integrate ~/.fehbg option
# TODO: convert in array for diferent wallpapers in monitors
feh_wall        = wall_dir + "Photography/rocks.jpg"
xwinwrap_wall   = wall_dir + "nimated/'Lines - minimal.mp4'"
hset_wall       = "#906090"


#   SPECIALS
#---------------------------
# My special animated wallpaper
# represent system events with images
# + if true will overwrite above options
enable_specials = True

# Dynamic using feh
#>> depends on battery state:
#> values:
#       'qrot'              - minimal that rotate through qtile letters
special_feh = 'qrot'

# Dynamic using xwinwrap
#> values:
#   '?'             - description ; input: ?
special_xwinwrap = '?'

# Dynamic using hsetroot
#> values:
#   '?'             - description ; input: ?
special_hsetroot = '?'
