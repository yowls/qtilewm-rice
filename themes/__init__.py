import random
import subprocess
import settings.theming as settings
from settings.paths import qtile_themes


def init_color_scheme():
    """Set the color scheme based on user settings"""
    # color_scheme is the name and cs is the dict with colors

    if settings.color_scheme_random:
        settings.color_scheme = random.choice(settings.color_scheme_themes)

    cs = __import__(
        f"themes.colors.{settings.color_scheme}", fromlist=["colors"]
    ).colors

    # Save the color scheme in themes/colors/current for an easy import
    source = qtile_themes + f"colors/{settings.color_scheme}/__init__.py"
    location = qtile_themes + "colors/current.py"
    subprocess.Popen(f"ln -sf {source} {location}", shell=True)

    return cs


def init_statusbar():
    """Set Status-Bar and Dock
    Returns screens and fake_screens variable
    Depending on the theme, one of the two will by just a []
    """

    if settings.enable_statusbar:
        if settings.random_statusbar:
            settings.statusbar = random.choice(settings.statusbar_themes)

        if settings.random_dock:
            settings.dock = random.choice(settings.dock_themes)

        # load the status bar (including dock)
        setup = __import__(
            f"themes.statusbars.{settings.statusbar}", fromlist=["get_statusbar"]
        ).get_statusbar
        return setup(settings.dock)

    return [], None


def init_style_default():
    """Set extension default style"""

    if settings.enable_statusbar:
        init_extension_style = __import__(
            f"themes.statusbars.{settings.statusbar}.sources.style",
            fromlist=["extension_style"],
        ).extension_style
        return init_extension_style()

    # default extension_defaults
    return {
        "font": "sans",
        "fontsize": 12,
        "padding": 3,
        "foreground": "#f4f4f4",
        "background": "#272727",
    }
