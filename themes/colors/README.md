### 📖 Description
Here is the color schemes that i use in my config.<br>
Are written in python because that way I can import them from other files.<br>
Thats because are python dictionaries, and some of these are array producing a gradient color from top to bottom

To choose one just change the number in the [main config file](https://gitlab.com/yowls/qtwm/-/blob/master/config.py#L33), which is a referece for import the theme.

<br>
<br>

## 🖼️ Previews
| Scheme | Name |
| ----- | ------ |
| <img src="https://i.imgur.com/vbTSKmV.png" height=250px> | Greene |
| <img src="https://i.imgur.com/M3q6P9S.png" height=250px> | Nihilist |
| <img src="https://i.imgur.com/TSANOJu.png" height=250px> | Soft |
| <img src="https://i.imgur.com/AUGgLZm.png" height=250px> | Cloud |

<br>
<br>

#### > [[Details](https://gitlab.com/yowls/qtwm/-/wikis/Color-Schemes)]

