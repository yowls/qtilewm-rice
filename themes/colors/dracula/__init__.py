#    COLOR SCHEME
#---------------------------
# define color palette
colors = {
    'bg':       "#282a36",
    'bg-grad':  ["#282a36", "#44475a"],

    'fg':           "#f8f8f2",
    'fg-inactive':  "#6272a4",

    # Special
    'accent':   "#44475a",
    'alert':    "#ff5555",

    # Palette
    'color1':   "#ff79c6",
    'color2':   "#50fa7b",
    'color3':   "#f1fa8c",
    'color4':   "#ffb86c",
    'color5':   "#bd93f9",
    'color6':   "#8be9fd"
}
