#    COLOR SCHEME
#---------------------------
# define color palette
colors = {
    'bg':       "#475c6c",
    'bg-grad':  ["#475c6c", "#495057"],

    'fg':           "#faf9f9",
    'fg-inactive':  "#6c757d",

    # Special
    'accent':   "#A89FE9",
    'alert':    "#F7B7D9",

    # Scheme
    'color1':   "#E9A9E7",
    'color2':   "#b7e4c7",
    'color3':   "#F7E8D2",
    'color4':   "#B8CCF8",
    'color5':   "#A89FE9",
    'color6':   "#FBC8D4"
}
