#    COLOR SCHEME
#---------------------------
# define color palette
colors = {
    'bg':       "#141414",
    'bg-grad':  ["#141414", "#141414"],

    'fg':           "#f5f6fa",
    'fg-inactive':  "#979dac",

    # Special
    'accent':   "#2B6ABC",
    'alert':    "#ffdd00",

    # Scheme
    'color1':   "#ffdd00",
    'color2':   "#003D82",
    'color3':   "#2B6ABC",
    'color4':   "#002F63",
    'color5':   "#240046",
    'color6':   "#0C53A6"
}
