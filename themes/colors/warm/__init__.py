#    COLOR SCHEME
#---------------------------
# define color palette
colors = {
    'bg':       "#353535",
    'bg-grad':  ["#353535", "#353535"],

    'fg':           "#faf9f9",
    'fg-inactive':  "#555b6e",

    # Special
    'accent':   "#ee6055",
    'alert':    "#dd2c2f",

    # Scheme
    'color1':   "#dd2c2f",
    'color2':   "#ffdab9",
    'color3':   "#fbc4ab",
    'color4':   "#f8ad9d",
    'color5':   "#f4978e",
    'color6':   "#f08080"
}
