#    COLOR SCHEME
#---------------------------
# define color palette
colors = {
    'bg':       "#0b132b",
    'bg-grad':  ["#0b132b", "#0b132b", "#293241"],

    'fg':           "#f5f6fa",
    'fg-inactive':  "#c4c4c8",

    # Special
    'accent':   "#40394a",
    'alert':    "#ffffff",

    # Scheme
    'color1':   "#ffffff",
    'color2':   "#22333b",
    'color3':   "#4c5c68",
    'color4':   "#3a506b",
    'color5':   "#202040",
    'color6':   "#1c2541"
}
