from libqtile import widget
from themes.colors.current import colors
from .style import *
from .callbacks import *


##############
# SEPARATORS #
##############

separation_high = widget.Sep(background=colors["bg"], padding=30, linewidth=0)
separation_medium = widget.Sep(background=colors["bg"], padding=15, linewidth=0)
separation_low = widget.Sep(background=colors["bg"], padding=5, linewidth=0)

spacer = widget.Spacer(background=colors["bg"])


#############
# HIDE DOCK #
#############

hide_bar = widget.TextBox(
    text="",
    background=colors["bg"],
    foreground=colors["fg"],
    mouse_callbacks={"Button1": hide_bar, "Button2": "", "Button3": ""},
    **textbox_defaults
)


################
# APPLICATIONS #
################

browser = widget.TextBox(
    text="",
    background=colors["bg"],
    foreground=colors["fg"],
    mouse_callbacks={"Button1": launch_firefox, "Button2": "", "Button3": ""},
    **textbox_defaults
)

file_manager = widget.TextBox(
    text="",
    background=colors["bg"],
    foreground=colors["fg"],
    mouse_callbacks={"Button1": launch_dolphin, "Button2": "", "Button3": ""},
    **textbox_defaults
)

text_editor = widget.TextBox(
    text="",
    background=colors["bg"],
    foreground=colors["fg"],
    mouse_callbacks={"Button1": launch_vsc, "Button2": "", "Button3": ""},
    **textbox_defaults
)

terminal = widget.TextBox(
    text="",
    background=colors["bg"],
    foreground=colors["fg"],
    mouse_callbacks={"Button1": launch_kitty, "Button2": "", "Button3": ""},
    **textbox_defaults
)

telegram = widget.TextBox(
    text="",
    background=colors["bg"],
    foreground=colors["fg"],
    mouse_callbacks={"Button1": launch_telegram, "Button2": "", "Button3": ""},
    **textbox_defaults
)
