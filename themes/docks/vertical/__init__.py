#################
# VERTICAL DOCK #
#################
# set a centered vertical bar with applications

from libqtile import bar
from .sources import style, widgets


def init_dock():
    return bar.Bar(
        [
            widgets.center_padding,  # Huge padding

            widgets.rofi_launcher,
            widgets.separation_medium,

            widgets.rofi_windows,
            widgets.separation_medium,

            widgets.rofi_scripts,

            widgets.separator,  # division

            widgets.terminal,
            widgets.separation_medium,

            widgets.browser,
            widgets.separation_medium,

            widgets.file_manager,
            widgets.separation_medium,

            widgets.text_editor,
            widgets.separation_medium,

            widgets.telegram,
            widgets.separation_high,

            widgets.hide_bar,

            widgets.spacer,
        ],
        style.dock_width,
        margin=style.dock_margin,
        background=style.bg_color
    )
