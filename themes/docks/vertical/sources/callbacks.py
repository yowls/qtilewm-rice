###################
# MOUSE CALLBACKS #
###################
# Functions executed when touching a widget in bar

# Bar
def hide_bar(qtile):
    qtile.cmd_hide_show_bar(position="left")


# Rofi Launcher
def launcher(qtile):
    qtile.cmd_spawn("rofi -show drun")


def launcher_window(qtile):
    qtile.cmd_spawn("rofi -show window")


def launcher_script(qtile):
    qtile.cmd_spawn("rofi -show drun")


# Applications
def launch_kitty(qtile):
    qtile.cmd_spawn("kitty")


def launch_firefox(qtile):
    qtile.cmd_spawn("firefox")


def launch_dolphin(qtile):
    qtile.cmd_spawn("dolphin")


def launch_vsc(qtile):
    qtile.cmd_spawn("codium")


def launch_telegram(qtile):
    qtile.cmd_spawn("telegram")
