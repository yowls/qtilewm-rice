from themes.colors.current import colors

############
# SETTINGS #
############
# Define general style settings

# => Measurements (in pixels)
dock_width = 25
dock_margin = 5
margin_x = 3

# => Locations
# FIXME: replace with settings.path
default_icon_location = "~/.config/qtile/assets/file_icons/"

# => Colors
bg_color = colors["bg"]  # Dock background


##############
# SET CONFIG #
##############

default_separation = {"padding": 20, "linewidth": 0}

textbox_defaults = {}
