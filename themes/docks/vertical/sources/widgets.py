from libqtile import widget
from themes.colors.current import colors
from .style import *
from .callbacks import *


##############
# SEPARATORS #
##############

separation_high = widget.Sep(background=colors["bg"], padding=30, linewidth=0)
separation_medium = widget.Sep(background=colors["bg"], padding=15, linewidth=0)
separation_low = widget.Sep(background=colors["bg"], padding=5, linewidth=0)

spacer = widget.Spacer(background=colors["bg"])

center_padding = widget.Sep(background=colors["bg"], padding=170, linewidth=0)

# FIXME: margin_x, default_icon_location
separator = widget.Image(
    scale=False,
    margin_x=margin_x,
    filename=default_icon_location + "separation.png",
    background=colors["bg"],
)


#############
# HIDE DOCK #
#############

# FIXME: margin_x, default_icon_location
hide_bar = widget.Image(
    scale=False,
    margin_x=margin_x,
    filename=default_icon_location + "left-arrows.png",
    background=colors["bg"],
    mouse_callbacks={"Button1": hide_bar, "Button2": "", "Button3": ""},
)


########
# ROFI #
########

# FIXME: margin_x, default_icon_location
rofi_launcher = widget.Image(
    scale=False,
    margin_x=margin_x,
    filename=default_icon_location + "applications.png",
    background=colors["bg"],
    mouse_callbacks={"Button1": launcher, "Button2": "", "Button3": ""},
)

rofi_windows = widget.Image(
    scale=False,
    margin_x=margin_x,
    filename=default_icon_location + "windows.png",
    background=colors["bg"],
    mouse_callbacks={"Button1": launcher_window, "Button2": "", "Button3": ""},
)

rofi_scripts = widget.Image(
    scale=False,
    margin_x=margin_x,
    filename=default_icon_location + "run.png",
    background=colors["bg"],
    mouse_callbacks={"Button1": launcher_script, "Button2": "", "Button3": ""},
)


################
# APPLICATIONS #
################

# FIXME: margin_x, default_icon_location
browser = widget.Image(
    scale=False,
    margin_x=margin_x,
    filename=default_icon_location + "browser.png",
    background=colors["bg"],
    mouse_callbacks={"Button1": launch_firefox, "Button2": "", "Button3": ""},
)

file_manager = widget.Image(
    scale=False,
    margin_x=margin_x,
    filename=default_icon_location + "folder.png",
    background=colors["bg"],
    mouse_callbacks={"Button1": launch_dolphin, "Button2": "", "Button3": ""},
)

text_editor = widget.Image(
    scale=False,
    margin_x=margin_x,
    filename=default_icon_location + "text-editor.png",
    background=colors["bg"],
    mouse_callbacks={"Button1": launch_vsc, "Button2": "", "Button3": ""},
)

terminal = widget.Image(
    scale=False,
    margin_x=margin_x,
    filename=default_icon_location + "terminal.png",
    background=colors["bg"],
    mouse_callbacks={"Button1": launch_kitty, "Button2": "", "Button3": ""},
)

# FIXME: margin_x, default_icon_location
telegram = widget.Image(
    scale=False,
    margin_x=margin_x,
    filename=default_icon_location + "telegram.png",
    background=colors["bg"],
    mouse_callbacks={"Button1": launch_telegram, "Button2": "", "Button3": ""},
)
