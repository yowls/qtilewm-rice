from libqtile.config import Screen
from library.helpers import count_monitors
from settings.theming import statusbar_variant, enable_dock
from . import simple, compost, multi


def get_statusbar(dock):
    """Initialize the statusbar based on user settings"""

    screens = []
    fake_screens = None

    if statusbar_variant == "multi":
        # Widgets of main and second monitor
        main_widgets = simple.init()
        extended_widgets = multi.init()

        # Widgets in screens
        main_screen = Screen(top=main_widgets)
        second_screen = Screen(top=extended_widgets)

        # Dock option only for main monitor
        if enable_dock:
            init_dock = __import__(
                "themes.docks." + dock, fromlist=["init_dock"]
            ).init_dock
            dock_widgets = init_dock()

            if dock == "vertical":
                main_screen_dockerized = Screen(top=main_widgets, left=dock_widgets)
            else:
                main_screen_dockerized = Screen(top=main_widgets, bottom=dock_widgets)

            screens.append(main_screen_dockerized)
            screens.append(second_screen)
        else:
            screens.append(main_screen)
            screens.append(second_screen)

    elif statusbar_variant == "compost":
        # NOTE: need to recreate widgets for every monitors otherwise will fail

        for i in range(count_monitors):
            main_widgets = simple.init()

            # Widgets in main monitor
            if i == 0:
                extensive_widgets = compost.init()

                if enable_dock:
                    init_dock = __import__(
                        "themes.docks." + dock, fromlist=["init_dock"]
                    ).init_dock
                    dock_widgets = init_dock()

                    if dock == "vertical":
                        screens.append(
                            Screen(
                                top=main_widgets,
                                bottom=extensive_widgets,
                                left=dock_widgets,
                            )
                        )
                    elif dock == "horizontal":
                        # NOTE:Incompatible
                        screens.append(
                            Screen(top=main_widgets, bottom=extensive_widgets)
                        )
                else:
                    screens.append(Screen(top=main_widgets, bottom=extensive_widgets))
            else:
                # Replicate main widgets for other monitors
                screens.append(Screen(top=main_widgets))

    else:
        # statusbar_variant == "simple"
        # NOTE: need to recreate widgets for every monitors otherwise will fail

        for i in range(count_monitors):
            widgets = simple.init()

            # Setup Dock only in main monitor
            if i == 0 and enable_dock:
                init_dock = __import__(
                    "themes.docks." + dock, fromlist=["init_dock"]
                ).init_dock
                dock_widgets = init_dock()

                if dock == "vertical":
                    # Vertical left
                    screens.append(Screen(top=widgets, left=dock_widgets))
                else:
                    # Horizontal bottom
                    screens.append(Screen(top=widgets, bottom=dock_widgets))
            else:
                screens.append(Screen(top=widgets))

    return screens, fake_screens
