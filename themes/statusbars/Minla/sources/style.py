from themes.colors.current import colors

############
# SETTINGS #
############
# Define general style settings

# => Measurements (in pixels)
height = 25
margin = [0, 20, 5, 20]  # top, right, bottom, left

# => Select Font
# TODO: move to settings/theming?
font_icon = "FuraCode Nerd Font"
font_text = "Open Sans Bold"

# => Font sizes
font_text_size = 10
font_icon_size = 16
font_groups_size = 16
systray_icon_size = 16

# => Colors
bg_color = colors["bg"]  # Status bar background


##############
# SET CONFIG #
##############

# Widgets base
icon_defaults = {"font": font_icon, "fontsize": font_icon_size, "padding": 4}
widget_defaults = {"padding": 0, "font": font_text, "fontsize": font_text_size}


def extension_style():
    extension_defaults = widget_defaults.copy()
    extension_defaults["background"] = colors["bg"]
    extension_defaults["foreground"] = colors["fg"]
    return extension_defaults
