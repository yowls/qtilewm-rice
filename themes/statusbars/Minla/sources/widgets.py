from libqtile import widget
from themes.colors.current import colors
from settings.paths import qtile_dir
from .style import *
from .callbacks import *

import socket


##############
# SEPARATORS #
##############

separation_high = widget.Sep(background=colors["bg"], padding=30, linewidth=0)
separation_medium = widget.Sep(background=colors["bg"], padding=15, linewidth=0)
separation_low = widget.Sep(background=colors["bg"], padding=5, linewidth=0)

spacer = widget.Spacer(background=colors["bg"])


###########
# BATTERY #
###########

battery_icon = widget.TextBox(
    text="", background=colors["bg"], foreground=colors["color3"], **icon_defaults
)

battery_text = widget.Battery(
    background=colors["bg"],
    foreground=colors["fg"],
    low_foreground=colors["fg-inactive"],
    low_percentage=0.2,
    format="{percent:2.0%}",
    show_short_text=True,
    charge_char="+",
    discharge_char="-",
    empty_char="x",
    full_char="f",
    unknown_char="?",
    update_interval=60,
    **widget_defaults,
)


#########
# CLOCK #
#########

clock_icon = widget.TextBox(
    text="", background=colors["bg"], foreground=colors["color2"], **icon_defaults
)

clock_text = widget.Clock(
    background=colors["bg"], foreground=colors["fg"], format="%H:%M", **widget_defaults
)


###########
# CRYPTOS #
###########

bitcoin_icon = widget.TextBox(
    text="", background=colors["bg"], foreground=colors["accent"], **icon_defaults
)

bitcoin_text = widget.BitcoinTicker(
    background=colors["bg"],
    foreground=colors["fg"],
    currency="USD",
    url=None,
    update_interval=600,
    **widget_defaults,
)


########
# DATE #
########

date_icon = widget.TextBox(
    text="", background=colors["bg"], foreground=colors["color6"], **icon_defaults
)

date_text = widget.Clock(
    background=colors["bg"],
    foreground=colors["fg"],
    format="%a %d",
    **widget_defaults,
)


########
# DISK #
########

diskfree_icon = widget.TextBox(
    text="", background=colors["bg"], foreground=colors["accent"], **icon_defaults
)

diskfree_text = widget.DF(
    background=colors["bg"],
    foreground=colors["fg"],
    warn_color=colors["alert"],
    format="{uf}{m}|{r:.0f}%",
    partition="/",
    measure="G",
    warn_space=2,
    visible_on_warn=False,
    update_interval=600,
    **widget_defaults,
)


########
# EXIT #
########

exit_icon = widget.TextBox(
    text="", background=colors["bg"], foreground=colors["accent"], **icon_defaults
)

exit_text = widget.QuickExit(
    background=colors["bg"],
    foreground=colors["fg"],
    default_text=" Exit",
    countdown_format="{} seconds",
    countdown_start=5,
    timer_interval=1,
    **widget_defaults,
)


##########
# GROUPS #
##########

groups = widget.GroupBox(
    # Measures
    padding=5,
    margin=0,
    borderwidth=3,
    spacing=None,
    center_aligned=True,
    rounded=True,
    # Format
    fmt="[{}]",
    highlight_method="block",
    block_highlight_text_color=None,
    urgent_alert_method="border",
    hide_unused=True,
    disable_drag=True,
    # Fonts
    font=font_icon,
    fontsize=font_groups_size,
    # Colors
    background=colors["bg"],
    foreground=colors["fg"],
    active=colors["fg"],
    inactive=colors["fg-inactive"],
    highlight_color=colors["accent"],
    urgent_border=colors["alert"],
    urgent_text=colors["bg"],
    this_screen_border=colors["color2"],
    other_screen_border=colors["color2"],
    this_current_screen_border=colors["color4"],
    other_current_screen_border=colors["color4"],
)


###########
# LAYOUTS #
###########

layouts_icon = widget.CurrentLayoutIcon(
    scale=0.6, background=colors["bg"], foreground=colors["accent"], **icon_defaults
)


##########
# MEMORY #
##########

memory_icon = widget.TextBox(
    text="",
    background=colors["bg"],
    foreground=colors["color4"],
    mouse_callbacks={"Button1": launch_htop, "Button2": "", "Button3": ""},
    **icon_defaults,
)

memory_text = widget.Memory(
    background=colors["bg"],
    foreground=colors["fg"],
    measure_mem="M",
    update_interval=3.0,
    mouse_callbacks={"Button1": launch_htop, "Button2": "", "Button3": ""},
    **widget_defaults,
)


###########
# NETWORK #
###########

network_icon = widget.TextBox(
    text="", background=colors["bg"], foreground=colors["accent"], **icon_defaults
)

network_text = widget.Net(
    background=colors["bg"],
    foreground=colors["fg"],
    format="{down}↓ ↑{up}",
    use_bits=False,
    update_interval=3,
    **widget_defaults,
)


##############
# POWER MENU #
##############

power_menu = widget.TextBox(
    text="⏻",
    background=colors["bg"],
    foreground=colors["color1"],
    mouse_callbacks={"Button1": "", "Button2": "", "Button3": ""},
    **icon_defaults,
)


##########
# PROMPT #
##########

prompt = widget.Prompt(
    background=colors["bg"],
    foreground=colors["fg"],
    cursor=True,
    cursor_color=colors["accent"],
    cursorblink=1,
    record_history=True,
    ignore_dups_history=True,
    max_history=100,
    visual_bell_color=colors["color1"],
    visual_bell_time=0.2,
    prompt=f"{socket.gethostname()}: ",
    **widget_defaults,
)


###############
# SYSTEM TRAY #
###############

system_tray = widget.Systray(
    background=colors["bg"], padding=2, icon_size=systray_icon_size
)


##################
# THERMAL SENSOR #
##################

thermal_sensor_icon = widget.TextBox(
    text="", background=colors["bg"], foreground=colors["accent"], **icon_defaults
)

thermal_sensor_text = widget.ThermalSensor(
    background=colors["bg"],
    foreground=colors["fg"],
    metric=True,
    show_tag=False,
    threshold=70,
    update_interval=10,
    **widget_defaults,
)


###############
# TOGGLE DOCK #
###############

# FIXME: change icon file path
toggle_dock = widget.Image(
    background=colors["bg"],
    foreground=colors["accent"],
    scale=False,
    margin_y=5,
    filename=qtile_dir + "themes/icons/xx/yy/list.png",
    mouse_callbacks={"Button1": toggle_bar, "Button2": "", "Button3": ""},
)


#############
# WALLPAPER #
#############

wallpaper_icon = widget.TextBox(
    text="", background=colors["bg"], foreground=colors["accent"], **icon_defaults
)

# TODO: change wallpaper dir to a variable in path
wallpaper_text = widget.Wallpaper(
    background=colors["bg"],
    foreground=colors["fg"],
    wallpaper=None,
    directory="~/library/Wallpaper/Photography/",
    label=None,
    option="fill",
    random_selection=True,
    wallpaper_command=["feh", "--bg-fill"],
    **widget_defaults,
)


###############
# WINDOW NAME #
###############

window_name = widget.WindowName(
    background=colors["bg"],
    foreground=colors["fg"],
    max_chars=30,
    format="{state}{name}",
    for_current_screen=False,
    empty_group_string="Desktop",
    **widget_defaults,
)


###############
# WINDOW TABS #
###############

window_tab = widget.WindowTabs(
    background=colors["bg"],
    foreground=colors["fg"],
    selected=("<b>", "</b>"),
    separator=" 🫐 ",
    max_chars=30,
    **widget_defaults,
)
