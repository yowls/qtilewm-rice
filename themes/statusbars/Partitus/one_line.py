# Load qtile libs for set the bar
from libqtile.config import Screen
from libqtile import bar, widget

# Load my custom config
from themes.statusbar.Partitus.sources.style import *
from themes.statusbar.Partitus.sources.callbacks import run, toggle_bar, launch_htop


#    DEFINE STATUS BAR
#---------------------------
# creates two separate bars

# => LEFT SIDE
def left_bar(colors):
    qtile_bar = bar.Bar([
            widget.Sep(
                background= colors['bg'],
                padding = 15,
                linewidth = 0
            ),
        # Toggle Bar
            widget.Image(
                background = colors['bg'],
                foreground= colors['accent'],
                scale = False,
                margin_y = 5,
                filename = "~/.config/qtile/assets/file_icons/list.png",
                mouse_callbacks = {'Button1': toggle_bar, 'Button2': '', 'Button3': ''}
            ),
            widget.Sep(background= colors['bg'], **default_separation),
        #---
            widget.GroupBox(
                background= colors['bg'],
                foreground= colors['fg'],
                active = colors['fg'],
                inactive = colors['fg-inactive'],
                highlight_color = colors['accent'],
                urgent_border = colors['alert'],
                urgent_text = colors['bg'],
                this_screen_border = colors['color2'],
                other_screen_border = colors['color2'],
                this_current_screen_border = colors['color4'],
                other_current_screen_border = colors['color4'],
                **workspaces_config
            ),
            widget.Sep(background= colors['bg'], **default_separation),
        ], bar_height, margin=bar_margin, background=colors['bg'], opacity=1)
    return qtile_bar

# => RIGHT SIDE
def right_bar(colors):
    qtile_bar = bar.Bar([
        # System Tray
            widget.Systray(
                background  = colors['bg'],
                icon_size   = sys_tray_icon_size,
                padding = 2
            ),
            widget.Sep(background= colors['bg'], **default_separation),
        #---
        # BATTERY
            widget.TextBox(
                text = '',
                background = colors['bg'],
                foreground = colors['color3'],
                **icon_defaults
            ),
            widget.Sep(
                background= colors['bg'],
                padding = 5,
                linewidth = 0
            ),
            widget.Battery(
                background = colors['bg'],
                foreground = colors['fg'],
                low_foreground = colors['fg-inactive'],
                low_percentage = 0.2,
                format = '{percent:2.0%}',
                show_short_text = True,
                charge_char = '+',
                discharge_char = '-',
                empty_char = 'x',
                full_char = 'f',
                unknown_char = '?',
                update_interval = 60,
                **widget_defaults
            ),
            widget.Sep(background= colors['bg'], **default_separation),
        #---
        # DATE
            widget.TextBox(
                text = '',
                background = colors['bg'],
                foreground = colors['color6'],
                **icon_defaults
            ),
            widget.Sep(
                background= colors['bg'],
                padding = 5,
                linewidth = 0
            ),
            widget.Clock(
                background = colors['bg'],
                foreground = colors['fg'],
                format = "%a %d",
                **widget_defaults
            ),
            widget.Sep(background= colors['bg'], **default_separation),
        #---
        # CLOCK
            widget.TextBox(
                text = '',
                background = colors['bg'],
                foreground = colors['color2'],
                **icon_defaults
            ),
            widget.Sep(
                background= colors['bg'],
                padding = 5,
                linewidth = 0
            ),
            widget.Clock(
                background = colors['bg'],
                foreground = colors['fg'],
                format = "%H:%M",
                **widget_defaults
            ),
            widget.Sep(background= colors['bg'], **default_separation),
        #---
        # CLOCK
            widget.TextBox(
                text = '⏻',
                background = colors['bg'],
                foreground = colors['color1'],
                mouse_callbacks = {'Button1': '', 'Button2': '', 'Button3': ''},
                **icon_defaults
            ),
        #---
            widget.Sep(
                background = colors['bg'],
                padding = 15,
                linewidth = 0
            )
        ], bar_height, margin=bar_margin, background=colors['bg'], opacity=1)
    return qtile_bar


# Split the same bar in N monitors
def split_screens(_dock, color_scheme, _monitors):
    screens = []
    fake_screens = []

    # Replicate the same bar for other monitors
    # need to recreate widgets for every monitors otherwise will fail
    # define a separation between widgets monitors for the positioning
    SEPARATION = 890
    for i in range(_monitors):
        widgets_left = left_bar(color_scheme)
        widgets_right = right_bar(color_scheme)

        position_left_x = i*SEPARATION
        position_right_x = 866 + i*SEPARATION

        # Dock option only for main monitor
        if _dock != "none":
            pass

        else:
            widget_in_bar_left  = Screen(top=widgets_left, x=position_left_x, y=0, width=300, height=30)
            widget_in_bar_right = Screen(top=widgets_right, x=position_right_x, y=0, width=500, height=30)
            fake_screens.append(widget_in_bar_left)
            fake_screens.append(widget_in_bar_right)

    return screens, fake_screens
