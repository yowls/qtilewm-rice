#    GENERAL SETTINGS
#---------------------------
# define general style settings
# measurements are in pixels (px)

bar_height = 25
bar_margin = [0,15,5,15]

# => Select Font
font_icon = "Font Awesome"
font_text = "Open Sans Bold"

# => Font sizes
font_text_size = 12
font_icon_size = 30
font_groups_size = 21
sys_tray_icon_size = 18

# => Groups settings
highlight_method = "block"
urgent_alert_method = "border"
group_border_width = 3
disable_drag_groups = True
groups_hide_empty = True


#    BUILD CONFIG
#---------------------------
# set the config in dictionaries for easier use
icon_defaults = dict(
    font = font_icon,
    fontsize = font_icon_size,
    padding = 4
)

workspaces_config = dict(
    padding = 5,
    margin  = 0,
    borderwidth = group_border_width,
    spacing = None,
    center_aligned = True,
    rounded = True,

    font     = font_icon,
    fontsize = font_groups_size,

    highlight_method = highlight_method,
    block_highlight_text_color = None,
    urgent_alert_method = urgent_alert_method,

    hide_unused = groups_hide_empty,
    disable_drag = disable_drag_groups
)

default_separation = dict(
    padding= 30,
    linewidth= 0
)

widget_defaults = dict(
    padding = 0,
    font = font_text,
    fontsize = font_text_size
)

def extension_style (color_scheme):
    extension_defaults = widget_defaults.copy()
    extension_defaults['background'] = color_scheme['bg']
    extension_defaults['foreground'] = color_scheme['fg']
    return extension_defaults
