### 📖 Description
This is my collection of themes for the built-in Qtile bar<br>
The idea is to make it as modular as possible,<br>
to be able to import any theme easily

To choose one just change the number in the [main config file](https://gitlab.com/yowls/qtwm/-/blob/master/config.py#L29), which is a referece for import the theme.<br>
Besides, you can choose the [variant](https://gitlab.com/yowls/qtwm/-/blob/master/Bars/init.py#L25) which will display another bar depending on the case

<br>
<br>

## 🖼️ Previews
...

<br>
<br>

#### > [[Details](https://gitlab.com/yowls/qtwm/-/wikis/Status-Bar)]

