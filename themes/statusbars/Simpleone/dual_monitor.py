# Load qtile libs for set the bar
from libqtile.config import Screen
from libqtile import bar, widget

# Load my custom config
from themes.statusbar.Simpleone.one_line import init_bar as init_main_bar
from themes.statusbar.Simpleone.sources.style import *
# from themes.statusbar.Simpleone.sources.callbacks import ..


#    DEFINE STATUS BAR
#---------------------------
# create a simple and compacted status bar
# with diferent widgets in two monitors

def init_extended_bar(colors):
    qtile_bar = bar.Bar([
        # LAYOUTS
            widget.CurrentLayoutIcon(
                scale = 0.6,
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
        #---
        # WORKSPACES
            widget.GroupBox(
                background= colors['bg'],
                foreground= colors['fg'],
                active = colors['fg'],
                inactive = colors['fg-inactive'],
                highlight_color = colors['accent'],
                urgent_border = colors['alert'],
                urgent_text = colors['bg'],
                this_screen_border = colors['color2'],
                other_screen_border = colors['color2'],
                this_current_screen_border = colors['color5'],
                other_current_screen_border = colors['color5'],
                **workspaces_config
            ),

        #--- MIDDLE SIDE
            widget.Spacer(background  = colors['bg']),
            widget.WindowName(
                background = colors['bg'],
                foreground = colors['fg'],
                max_chars = 35,
                **widget_defaults
            ),
            widget.Spacer(background  = colors['bg']),

        #--- RIGHT SIDE
        # Bitcoin
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.BitcoinTicker(
                background = colors['accent'],
                foreground = colors['fg'],
                currency    = 'USD',
                update_interval = 600,
                url = None,
                **widget_defaults
            ),
        #---
        # Disk Free
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.DF(
                background = colors['accent'],
                foreground = colors['fg'],
                warn_color  = colors['color1'],
                format      = '{uf}{m}|{r:.0f}%',
                partition   = '/',
                measure     = 'G',
                warn_space  = 2,
                visible_on_warn = False,
                update_interval = 600,
                **widget_defaults
            ),
        #---
        # NOTIFY
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.Net(
                background = colors['accent'],
                foreground = colors['fg'],
                format = '{down}↓ ↑{up}',
                update_interval = 3,
                use_bits = False,
                **widget_defaults
            ),
        #---
        # THERMAL SENSORS
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.ThermalSensor(
                background = colors['accent'],
                foreground = colors['fg'],
                metric      = True,
                show_tag    = False,
                threshold   = 70,
                update_interval = 10,
                **widget_defaults
            ),
        #---
        # CLOCK
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.Clock(
                background = colors['accent'],
                foreground = colors['fg'],
                format = "%H:%M",
                **widget_defaults
            ),
        #---
        # Quick Exit
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                **icon_defaults
            ),
            widget.QuickExit(
                background = colors['accent'],
                foreground = colors['fg'],
                countdown_format = '{} seconds',
                countdown_start = 5,
                timer_interval = 1,
                default_text = ' Exit',
                **widget_defaults
            ),
        #---
            widget.Sep(
                background= colors['accent'],
                padding = 15,
                linewidth = 0
            )
        ], bar_height, margin=bar_margin, background=colors['bg'], opacity=1)
    return qtile_bar


# Main bar is defined in the one-line file
# so i just create another bar and append to the original
def split_screens(_dock, color_scheme, _monitors):
    screens = []
    fake_screens = []

    # Widgets of main and second monitor
    main_widgets = init_main_bar(color_scheme)
    extended_widgets = init_extended_bar(color_scheme)

    # Widgets in screens
    main_screen = Screen(bottom=main_widgets)
    second_screen = Screen(bottom=extended_widgets)

    # Dock option only for main monitor
    if _dock == "none":
        # Put both widgets in monitors
        screens.append(main_screen)
        screens.append(second_screen)

    else:
        setup_dock = __import__('Bars.docks.'+_dock, fromlist=['init_dock']).init_dock
        dock_widgets = setup_dock(color_scheme)

        if _dock == "vertical":
            main_screen_dockerized = Screen(bottom=main_widgets, left=dock_widgets)
        elif _dock == "horizontal":
            main_screen_dockerized = Screen(bottom=main_widgets, top=dock_widgets)

        screens.append(main_screen_dockerized)
        screens.append(second_screen)

    return screens, fake_screens
