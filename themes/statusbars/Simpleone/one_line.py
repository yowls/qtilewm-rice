# Load qtile libs for set the bar
from libqtile.config import Screen
from libqtile import bar, widget

# Load my custom config
from themes.statusbar.Simpleone.sources.style import *
# from themes.statusbar.Simpleone.sources.callbacks import ??


#    DEFINE STATUS BAR
#---------------------------
# set a very basic bar at bottom

def init_bar(colors):
    qtile_bar = bar.Bar([
        #==> LEFT SIDE
            widget.CurrentLayoutIcon(
                background = colors['accent'],
                foreground = colors['fg'],
                scale = 0.6,
                **widget_defaults
            ),
            widget.GroupBox(
                background= colors['bg'],
                foreground= colors['fg'],
                active = colors['fg'],
                inactive = colors['fg-inactive'],
                highlight_color = colors['bg'],
                urgent_border = colors['alert'],
                urgent_text = colors['bg'],
                this_screen_border = colors['color2'],
                other_screen_border = colors['color2'],
                this_current_screen_border = colors['accent'],
                other_current_screen_border = colors['color2'],
                **workspaces_config
            ),
            widget.Prompt(
                background = colors['bg'],
                foreground = colors['fg'],
                **widget_defaults
            ),
            widget.Spacer(background  = colors['bg']),

        #==> RIGHT SIDE
            widget.Systray(
                background  = colors['bg'],
                icon_size   = sys_tray_icon_size,
                padding = 4
            ),
            widget.TextBox(
                text = '',
                background = colors['bg'],
                foreground = colors['accent'],
                fontsize = font_icon_size,
                padding = 0
            ),
            widget.Sep(background= colors['accent'], padding = 5, linewidth= 0),
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                mouse_callbacks = {'Button1': '', 'Button2': '', 'Button3': ''},
                **icon_defaults
            ),
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                mouse_callbacks = {'Button1': '', 'Button2': '', 'Button3': ''},
                **icon_defaults
            ),
            widget.TextBox(
                text = '墳',
                background = colors['accent'],
                foreground = colors['fg'],
                mouse_callbacks = {'Button1': '', 'Button2': '', 'Button3': ''},
                **icon_defaults
            ),
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                mouse_callbacks = {'Button1': '', 'Button2': '', 'Button3': ''},
                **icon_defaults
            ),
            widget.Clock(
                format  = '%H:%M',
                background = colors['accent'],
                foreground = colors['fg'],
                **widget_defaults
            ),
            widget.TextBox(
                text = '',
                background = colors['accent'],
                foreground = colors['fg'],
                mouse_callbacks = {'Button1': '', 'Button2': '', 'Button3': ''},
                **icon_defaults
            )
        ], bar_height, margin=bar_margin, background=colors['bg'], opacity=0.95)
    return qtile_bar


# Split the same bar in a given monitors number
def split_screens(_dock, color_scheme, _monitors):
    screens = []
    fake_screens = []

    # Replicate the same bar for other monitors
    # need to recreate widgets for every monitors otherwise will fail
    for i in range(_monitors):
        widgets = init_bar(color_scheme)

        # Setup Dock only in main monitor
        if i == 0 and _dock != "none":
            setup_dock = __import__('Bars.docks.'+_dock, fromlist=['init_dock']).init_dock
            dock_widgets = setup_dock(color_scheme)

            if _dock == "vertical":
                # Vertical left
                screen_dockerized = Screen(bottom=widgets, left=dock_widgets)
            else:
                # Horizontal bottom
                screen_dockerized = Screen(bottom=widgets, top=dock_widgets)

            screens.append(screen_dockerized)

        else:
            widget_in_bar = Screen(bottom=widgets)
            screens.append(widget_in_bar)

    return screens, fake_screens
