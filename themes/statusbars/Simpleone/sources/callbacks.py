#    MOUSE CALLBACKS
#---------------------------
# functions executed when touching a widget in bar

def launch_htop(qtile):
    qtile.cmd_spawn("kitty -e htop")

def launch_volume(qtile):
    qtile.cmd_spawn("pavucontrol")

def audio_mute_toggle(qtile):
    qtile.cmd_spawn("volume toggle-mute")
